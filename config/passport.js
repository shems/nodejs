const ADMIN = 'admin';
var bcrypt = require('bcrypt');
var userModel = require('../models/UserSchema');
const ADMIN_PASSWORD = 'password';
const passport = require('passport');
const BearerStrategy = require('passport-http-bearer').Strategy;
const LocalStrategy = require('passport-local').Strategy;
var jwt = require('jsonwebtoken');

passport.use(new LocalStrategy((username, password, done) => {
  userModel.findOne({"username": username}, function (err, user) {
    
if (err) {
  return done(err);
}

if (!user) {
  return done(null, false);
  
}

if (user) {
  bcrypt.compare(password, user.password, (err, isMatch)=> {
    if (err) {
      return done(null,false)
    }
    if(isMatch)
    return done(null, jwt.sign(username, 'shhhhh'))
    else
    return done('Wrong Password')
  })
}

});
  
    
  }));
  
  passport.use(new BearerStrategy((token, done) => {
    try {
      const username  = jwt.decode(token, 'shhhhh');
      
      userModel.findOne({"username": username}, function (err, user) {
    
        if (err) {
          return done(err);
        }
        
        if (!user) {
          return done(null, false);
          
        }
        
        if (user) {
          return done(null, true);
        }
        
        });
    } catch (error) {
      done(null, false);
    }
  }));