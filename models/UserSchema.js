var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

var userSchema = new mongoose.Schema({
    username : {type: String, required: true, unique: true},
    image: String,
    email: { type: String,
      validate: {
        validator: function(v) {
          return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(v);
        },
        message: props => `${props.value} is not a valid email!`
      },
      required: [true, 'User email required']
    },
    password : {type: String, required: true},
    todos:[{type:mongoose.Schema.Types.ObjectId, ref:'todo'}]
  });

  userSchema.pre('save', function (next) {  
    var user = this;
    if (this.isModified('password') || this.isNew) {
      bcrypt.genSalt(10, function (err, salt) {
        if (err) {
          return next(err);
        }
        bcrypt.hash(user.password, salt, function(err, hash) {
          if (err) {
            return next(err);
          }
          user.password = hash;
          next();
        });
      });
    } else {
      return next();
    }
  });
  // userSchema.methods.comparePassword = (pw, done) => {  
  //   bcrypt.compare(pw, this.password, (err, isMatch)=> {
  //     if (err) {
  //       throw (err);
  //     }
  //     return(isMatch);
  //   }
  //   );
    
  // };

  module.exports = mongoose.model('user', userSchema);