var mongoose = require('mongoose');
var todoSchema = new mongoose.Schema({
    title : {type: String, required: true},
    desc : {type: String},
    start_date : {type: String, required: true},
    end_date : {type: String, required: true},
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'user'}
  });

  module.exports = mongoose.model('todo', todoSchema);
  