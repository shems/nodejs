var todoModel = require('../models/TodoSchema');
var express = require('express')
var router = express.Router();

router.post('/add', (req,res)=> {
console.log(req.body)
    var todo = new todoModel(req.body);
    todo.save(function (err) {
        if (err) { throw err; }
        console.log('todo ajouté avec succès !');
      });
    })




router.get('/list', (req,res)=>{
    todoModel.find().populate('user').exec( function (err, todolist) {
        if (err) { throw err; }
        res.send(todolist);
     })
})

router.post('/affect/:id', (req,res)=> {

    var id= req.params.id;
        
        todoModel.update({_id: id},{user: req.body.user_id},function(err){
            if(err) console.log('error 2');
            
            else console.log('todo added')
          });
     
        })

router.post('/delete/:id',(req,res)=>{
    var id= req.params.id;
    todoModel.remove({_id: id},function(err){
        if(err) res.send(err)
        else console.log('todo removed')
    })
})

router.post('/update/:id', (req,res)=> {
    console.log(req.body)
    var id= req.params.id;
        var todo = new todoModel(req.body);
        todoModel.update({_id: id},req.body,function(err){
            if(err) res.send(err)
            else console.log('todo updated')
          });
        });

module.exports = router;