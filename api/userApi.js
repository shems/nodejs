var userModel = require('../models/UserSchema');
var todoModel = require('../models/TodoSchema');
const passport = require('passport');
require('../config/passport');



var path = require("path");
var cron = require('node-cron');
var express = require('express');
var router = express.Router();
var nodemailer = require("nodemailer");
var multer = require('multer');
const storage = multer.diskStorage({
  destination : function(req,file,callback){
    callback(null, './uploads');
  },
  filename: function(req,file,callback){
      callback(null,   Date.now() + '-' + file.originalname);
  }
});
const upload = multer({ storage : storage});

var task = cron.schedule('2 * * * *', () =>  {
  console.log('started from client');
}, {
  scheduled: false
});

// cron.schedule('* * * * *', () =>  {
//   console.log('auto start');
// });

router.post('/cronstart', (req,res)=>{
  task.start();
  console.log('cron Started')
})
router.post('/cronstop', (req,res)=>{
  task.stop();
  console.log('cron Stopped')
})

router.post('/img', upload.single('file'),  (req, res) => {
    var options = {
        root: path.join( ''),
        dotfiles: 'deny',
        headers: {
          'x-timestamp': Date.now(),
          'x-sent': true
        }
      }
    console.log(req.file);
    // res.sendFile(__dirname + req.file.path);
    res.sendFile(req.file.path, options, function (err) {
        if (err) {
          console.log(err)
        } else {
          console.log('Sent:', req.file.filename)
        }
      })
  })

router.post('/add', upload.single('file'), (req,res)=> {
  console.log(req.body);
  if(!req.body.email || !req.body.password) {
    res.send({ success: false, message: 'Please enter email and password.' });
  } else {
    var user = new userModel(req.body);
    user.image= req.file.filename;
    user.save(function (err) {
        if (err) { return res.json({ success: false, message: 'Email déja utilisé'}); }
        res.send({ success: true, message: 'Done ! ' });
      });
    }
    })

router.get('/list',passport.authenticate('bearer', { session: false }), (req,res)=>{
  console.log('the users list');
  
    userModel.find().populate('todos').exec(function (err, users) {
        if (err) { res.send(err.error) }
        res.send(users)
     })
})

router.post('/delete/:id',(req,res)=>{
    var id= req.params.id;
    userModel.remove({_id: id},function(err){
        if(err) res.send(err)
        else console.log('user removed')
    })
})

router.post('/update/:id', (req,res)=> {
    console.log(req.body)
    var id= req.params.id;
        userModel.update({_id: id},req.body,function(err){
            if(err) res.send(err)
            else console.log('user updated')
          });
        })

router.post('/affect/:id', (req,res)=> {
            var id= req.params.id;
            console.log(req.body.todo_id);
            todoModel.findOne({_id: req.body.todo_id}, function (err, todolist) {
                if (err) { res.send({message: 'error 1'});
                 }
                // res.send(todolist);
                console.log(todolist);
                userModel.update({_id: id},{$push: {todos: todolist._id}},function(err){
                    if(err) res.send({message: 'error 2'});
                    else console.log('todo added') // kifkif
                  });
             })
                })

router.post('/remove/:id', (req,res)=> {
        var id= req.params.id;
        console.log("ID TO UPDATE: "+ id);
       userModel.updateOne({_id: id},{$pull: {todos:  req.body.todo_id}},{ multi: true },function(err){
            if(err) console.log("ERROR:"+ err);
            else console.log('todo removed')
                          });
                        })                

router.get('/givemeimg/:id',(req,res)=>{
    console.log(req.params.id);
    // var options = {
    //     root: path.join( 'uploads'),
    //     dotfiles: 'deny',
    //     headers: {
    //       'x-timestamp': Date.now(),
    //       'x-sent': true
    //     }
    //   }
    res.sendFile(path.join(__dirname, '../uploads', req.params.id));
})

router.post('/sendemail', (req,res)=>{
    var testAccount = nodemailer.createTestAccount();
    console.log(testAccount);
    
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
               user: 'sanaa.shemseddine@gmail.com',
               pass: 'shemsMsa3sa3'
           }
       });
    var target= req.body.target;
    var msg= req.body.msg;

    var info = transporter.sendMail({
        from: '"Fred Foo 👻"', // sender address
        to: target, // list of receivers
        subject: "Hello ✔", // Subject line
        text: msg, // plain text body
        html: "<b>"+msg+"</b>" // html body
      },function(err,info){
        if(err)
        console.log(err)
      else
        console.log(info);
      });
})




router.post('/login', passport.authenticate('local', { session: false }),(req, res) => {
  res.send({
    token: req.user,
  });
  },
);
        module.exports = router;