var express = require("express");
var cors = require("cors");
var bodyParser = require("body-parser");
var uApi = require('./api/userApi');
var tApi = require('./api/todoApi');
var db = require('./db/database')

//needed for auth
var app = express();


//needed for the res and request pars
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({extended: true}));

app.get('/', function(req, res) {  
    res.send('Home');
  });
  
//the different APIs we have
app.use("/users",uApi);
app.use("/todo",tApi);



//the listen port
app.listen(3000);